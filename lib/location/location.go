package location

import (
	"database/sql"
	"fmt"
)

// TODO: Support Astro Object pointers instead of just ids.
type Location struct {
	id    int64
	ao_id int64
	name  string
}

func (l *Location) Print() string {
	return fmt.Sprintf("%d - %s near AO #%d", l.id, l.name, l.ao_id)
}

func GetAllLocations(db *sql.DB) ([]Location, error) {
	var locs []Location
	query := "SELECT tl_id, ao_id, name FROM Trade_Location"

	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int64
		var ao_id int64
		var name string
		if err := rows.Scan(&id, &ao_id, &name); err != nil {
			return nil, err
		}
		locs = append(locs, Location{id, ao_id, name})
	}

	return locs, nil
}
