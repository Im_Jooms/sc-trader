package good

import (
	"errors"
	"fmt"

	"database/sql"
)

type Good struct {
	id   int64
	name string
}

func (g *Good) Print() string {
	return fmt.Sprintf("%d - %s", g.id, g.name)
}

func GetAllGoods(db *sql.DB) ([]Good, error) {
	var goods []Good
	query := "SELECT g_id, name FROM Good ORDER BY name"

	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int64
		var name string
		if err := rows.Scan(&id, &name); err != nil {
			return nil, err
		}
		goods = append(goods, Good{id, name})
	}

	return goods, nil
}

func SellGood(db *sql.DB, name string) (map[string]float64, error) {
	if name == "" {
		return nil, errors.New("specify the name of a good to search for")
	}

	prices := make(map[string]float64)
	query := "SELECT tl.name, MAX(rp.price) FROM Recorded_Price rp " +
		"JOIN Trade_Location tl ON rp.tl_id = tl.tl_id " +
		"JOIN Good g ON rp.g_id = g.g_id " +
		"WHERE sold_to_station=1 " +
		fmt.Sprintf("AND g.name LIKE '%s' ", name) +
		"ORDER BY rp.price Desc "

	rows, err := db.Query(query)
	if err != nil {
		return nil, fmt.Errorf("error executing query %q", err)
	}
	defer rows.Close()

	for rows.Next() {
		var n sql.NullString
		var p sql.NullFloat64
		if err := rows.Scan(&n, &p); err != nil {
			return nil, fmt.Errorf("error scanning rows %q", err)
		}
		if !n.Valid {
			return nil, fmt.Errorf("error reading name for search %q", name)
		}
		if !p.Valid {
			return nil, fmt.Errorf("error reading price for search %q", name)
		}
		prices[n.String] = p.Float64
	}

	return prices, nil
}

func BuyGood(db *sql.DB, name string) (map[string]float64, error) {
	if name == "" {
		return nil, errors.New("specify the name of a good to search for")
	}

	prices := make(map[string]float64)
	query := "SELECT tl.name, MAX(rp.price) FROM Recorded_Price rp " +
		"JOIN Trade_Location tl ON rp.tl_id = tl.tl_id " +
		"JOIN Good g ON rp.g_id = g.g_id " +
		"WHERE sold_to_station=0 " +
		fmt.Sprintf("AND g.name LIKE '%s' ", name) +
		"ORDER BY rp.price Asc "

	rows, err := db.Query(query)
	if err != nil {
		return nil, fmt.Errorf("error executing query %q", err)
	}
	defer rows.Close()

	for rows.Next() {
		var n sql.NullString
		var p sql.NullFloat64
		if err := rows.Scan(&n, &p); err != nil {
			return nil, fmt.Errorf("error scanning rows %q", err)
		}
		if !n.Valid {
			return nil, fmt.Errorf("error reading name for search %q", name)
		}
		if !p.Valid {
			return nil, fmt.Errorf("error reading price for search %q", name)
		}
		prices[n.String] = p.Float64
	}

	return prices, nil
}
