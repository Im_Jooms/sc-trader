package astro_object

import "testing"

func TestPrint(t *testing.T) {
	var testCases = []struct {
		desc string
		id   int64
		name string
		want string
	}{

		{
			desc: "good print",
			id:   12,
			name: "face",
			want: "#12 face",
		}, {
			desc: "no name print",
			id:   12,
			want: "#12",
		}, {
			desc: "no id print",
			name: "cats",
			want: "#0 cats",
		}, {
			desc: "nothing print",
			want: "#0",
		},
	}

	for _, tt := range testCases {
		t.Run(tt.desc, func(t *testing.T) {
			ao := AstroObject{
				id:   tt.id,
				name: tt.name,
			}

			got := ao.Print()

			if got != tt.want {
				t.Errorf(
					"Print(%d,%s): expected %s, actual %v",
					tt.id,
					tt.name,
					tt.want,
					got)
			}
		})
	}
}
