package database

import (
	"io/ioutil"
	"os/user"
	"reflect"
	"testing"
)

func TestOpenCredFile(t *testing.T) {
	f, err := ioutil.TempFile("", "good")
	if err != nil {
		t.Fatalf("could not create a temp file for testing")
	}
	badJson, err := ioutil.TempFile("", "bad")
	if err != nil {
		t.Fatalf("could not create a temp file for testing")
	}
	emptyJson, err := ioutil.TempFile("", "empty")
	if err != nil {
		t.Fatalf("could not create a temp file for testing")
	}

	f.WriteString(`{
		"user": "u",
		"pass": "p",
		"host": "h",
		"dbname": "d"
	}`)
	emptyJson.WriteString(`
		"badkey": "u"
	}`)

	badJson.WriteString(`
		"badkey": "u"
	}`)

	ci := connectionInfo{
		User:   "u",
		Pass:   "p",
		Host:   "h",
		Dbname: "d",
	}

	var testCases = []struct {
		desc   string
		conn   *connectionInfo
		path   string
		user   *user.User
		hasErr bool
	}{{
		desc: "GoodCredFile",
		conn: &ci,
		path: f.Name(),
		user: &user.User{HomeDir: "/some/test"},
	}, {
		desc:   "BadCredFile",
		path:   badJson.Name(),
		hasErr: true,
	}, {
		desc:   "empty filename",
		path:   "",
		hasErr: true,
	}, {
		desc:   "EmptyCredFile",
		path:   emptyJson.Name(),
		hasErr: true,
	}}

	for _, tt := range testCases {
		t.Run(tt.desc, func(t *testing.T) {
			if tt.user != nil {
				curUser = func() (*user.User, error) {
					return tt.user, nil
				}
			} else {
				curUser = user.Current
			}
			conn, err := openCredFile(tt.path)
			if tt.hasErr != (err != nil) {
				eMsg := "no error"
				if tt.hasErr {
					eMsg = "an error"
				}
				t.Errorf("openCredFile(%s): error mismatch, expected %s, actual %v", tt.path, eMsg, err)
			}
			if !tt.hasErr && !reflect.DeepEqual(tt.conn, conn) {
				t.Errorf("openCredFile(%s): expected %v, actual %v", tt.path, tt.conn, conn)
			}
		})
	}
}
