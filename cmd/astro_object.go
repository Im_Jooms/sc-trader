package cmd

import (
	"fmt"
	"log"
	"strconv"

	"bitbucket.org/Im_Jooms/sc-trader/lib/astro_object"
	"bitbucket.org/Im_Jooms/sc-trader/lib/database"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(aoCmd)
	aoCmd.AddCommand(aoListCmd)
	aoCmd.AddCommand(aoAddCmd)
	aoCmd.AddCommand(aoAddressCmd)
}

var aoCmd = &cobra.Command{
	Use:   "ao",
	Short: "Commands relating to astronomical objects.",
	Long:  `Allows you to view more information about astronomical objects`,
}

var aoListCmd = &cobra.Command{
	Use:   "list",
	Short: "lists astronomical objects",
	Long:  `Provides a list of all astronomical objects in the database.`,
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		aos, err := astro_object.GetAllAstroObjects(db)
		if err != nil {
			log.Fatal("error retrieving data: ", err)
			return
		}
		for _, ao := range aos {
			fmt.Printf("%s\n", ao.Print())
		}
	},
}

var aoAddCmd = &cobra.Command{
	Use:   "add [name] [parent_ao_id]",
	Short: "adds an astronomical object to the db",
	Long:  `Provides a list of all astronomical objects in the database.`,
	Args:  cobra.RangeArgs(1,2),
	Run: func(cmd *cobra.Command, args []string) {
		var pId *int64
		if len(args) > 1 {
			id, err := strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				log.Fatal("the second argument must be an integer: got %q", args[1])
				return
			}
			pId = &id
		}
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		aoId, err := astro_object.AddAstroObject(db, args[0], pId)
		if err != nil {
			log.Fatal("error adding astro object: ", err)
			return
		}
		fmt.Printf("Added astro object %q with id %d", args[0], aoId)
	},
}

var aoAddressCmd = &cobra.Command{
	Use:   "address [aoId]",
	Short: "displays an astronomical object's address",
	Long:  `Provides a path to the astronomical object. Starting
	with the largest-known astronomical object and zooming into
	smaller and smaller related bodies. This is mostly based on
	orbits.`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		aoId, err := strconv.ParseInt(args[0], 10, 64)
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		allAOs, err := astro_object.GetAllAstroObjects(db)
		if err != nil {
			log.Fatal("error reading astro objects: ", err)
			return
		}

		reqAO, ok := allAOs[aoId]
		if !ok {
			log.Fatalf("error locating astro object '%d', %q", aoId, err)
		}

		fmt.Println(reqAO.GetAddress());
	},
}
