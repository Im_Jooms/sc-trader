package database

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"
)

var sqlOpen = sql.Open

const credFilePath = "~/.creds/db_creds.json"

type connectionInfo struct {
	User   string `json:"user"`
	Pass   string `json:"pass"`
	Host   string `json:"host"`
	Dbname string `json:"dbname"`
}

func Connect() (*sql.DB, error) {
	c, err := openCredFile(credFilePath)
	if err != nil {
		return nil, fmt.Errorf("could not open cred file: %s", err)
	}
	if c == nil {
		return nil, errors.New("no connection information in cred file")
	}

	connInfo := fmt.Sprintf("%s:%s@%s/%s", c.User, c.Pass, c.Host, c.Dbname)

	return sqlOpen("mysql", connInfo)
}

var curUser = user.Current

func openCredFile(filename string) (*connectionInfo, error) {
	if len(filename) == 0 {
		return nil, errors.New("cred file path is empty")
	}

	if filename[0] == '~' {
		usr, err := curUser()
		if err != nil {
			return nil, fmt.Errorf("could not retrieve current user: %v", err)
		}
		filename = filepath.Join(usr.HomeDir, filename[1:])
	}

	abPath, err := filepath.Abs(filename)
	if err != nil {
		return nil, fmt.Errorf("error evalutating path %q: %v", credFilePath, err)
	}

	if _, err := os.Stat(abPath); os.IsNotExist(err) {
		return nil, fmt.Errorf("could not locate cred file: %v", err)
	}

	b, err := ioutil.ReadFile(abPath)
	if err != nil {
		return nil, err
	}

	var c connectionInfo
	err = json.Unmarshal(b, &c)
	return &c, err
}
