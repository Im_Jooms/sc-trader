package cmd

import (
	"fmt"
	"log"

	"bitbucket.org/Im_Jooms/sc-trader/lib/database"
	"bitbucket.org/Im_Jooms/sc-trader/lib/location"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(locationCmd)
	locationCmd.AddCommand(locationListCmd)
}

var locationCmd = &cobra.Command{
	Use:   "location",
	Short: "Commands relating to trading locations.",
	Long: `Allows you to view more information about trading locations.
Including:
 * Getting a list of trade locations`,
}

var locationListCmd = &cobra.Command{
	Use:   "list",
	Short: "Get a list of trade locations.",
	Long:  `List all trade locations.`,
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		locs, err := location.GetAllLocations(db)
		if err != nil {
			log.Fatal("error retrieving data: ", err)
			return
		}
		for _, l := range locs {
			fmt.Printf("%s\n", l.Print())
		}
	},
}
