package astro_object

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
)

type AstroObject struct {
	Id        int64
	Name      string
	Parent    *AstroObject
}

func (ao *AstroObject) Print() string {
	return strings.TrimSpace(fmt.Sprintf("#%d %s", ao.Id, ao.Name))
}

func GetAllAstroObjects(db *sql.DB) (map[int64]*AstroObject, error) {
	aosMap := make(map[int64]*AstroObject)
	query := `SELECT ao_id, parent_ao_id, name
	FROM Astro_Object
	ORDER BY parent_ao_id`

	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id int64
		var sqlParentId sql.NullInt64
		var name string
		if err := rows.Scan(&id, &sqlParentId, &name); err != nil {
			return nil, err
		}
		var parentAO *AstroObject = nil
		if sqlParentId.Valid {
			parentAO = aosMap[sqlParentId.Int64]
		}
		aosMap[id] = &AstroObject{id, name, parentAO}
	}

	return aosMap, nil
}

func AddAstroObject(db *sql.DB, name string, parentId *int64) (int64, error) {
	insQ := "INSERT into Astro_Object (name"
	valQ := ") VALUES (?"
	endQ := ")"
	params := []interface{}{name}
	if parentId != nil {
		insQ += ", parent_ao_id"
		valQ += ", ?"
		params = append(params, *parentId)
	}
	query := strings.Join([]string{insQ, valQ, endQ}," ")
	r, err := db.Exec(query, params...)
	if err != nil {
		return 0, fmt.Errorf("there was an error adding astro objects: %v", err)
	}
	rows, err := r.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("there was an error getting rows affected when adding astro objects: %v", err)
	}
	if rows == 0 {
		return 0, errors.New("there were no astro objects added.")
	}
	return r.LastInsertId()
}

func (ao *AstroObject) GetAddress() string {
	outs := fmt.Sprintf("%s\n", ao.Print());
	for cAO := ao.Parent; cAO != nil; cAO = cAO.Parent {
		outs += fmt.Sprintf("%s\n", cAO.Print());
	}
	return outs
}
