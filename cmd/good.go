package cmd

import (
	"fmt"
	"log"

	"bitbucket.org/Im_Jooms/sc-trader/lib/database"
	"bitbucket.org/Im_Jooms/sc-trader/lib/good"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(goodCmd)
	goodCmd.AddCommand(goodListCmd)
	goodCmd.AddCommand(goodSellCmd)
	goodCmd.AddCommand(goodBuyCmd)
}

var goodCmd = &cobra.Command{
	Use:   "good",
	Short: "Commands relating to goods.",
	Long: `Allows you to view more information about goods.`,
}

var goodListCmd = &cobra.Command{
	Use:   "list",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		goods, err := good.GetAllGoods(db)
		if err != nil {
			log.Fatal("error retrieving data: ", err)
			return
		}
		for _, g := range goods {
			fmt.Printf("%s\n", g.Print())
		}
	},
}

var goodSellCmd = &cobra.Command{
	Use:   "sell [goodName]",
	Short: "The best places to sell a good.",
	Long:  `Returns the places you can sell a good at,
ordered by highest price.`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		goods, err := good.SellGood(db, args[0])
		if err != nil {
			log.Fatal("error retrieving data: ", err)
			return
		}
		if len(goods) == 0 {
			log.Fatal("There were no results found.")
		}
		for name, val := range goods {
			fmt.Printf("%s: %f\n", name, val)
		}
	},
}

var goodBuyCmd = &cobra.Command{
	Use:   "buy [goodName]",
	Short: "The best places to buy a good.",
	Long:  `Returns the places you can buy a good at,
ordered by lowest price.`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		db, err := database.Connect()
		if err != nil {
			log.Fatal("database connection error: ", err)
			return
		}

		goods, err := good.BuyGood(db, args[0])
		if err != nil {
			log.Fatal("error retrieving data: ", err)
			return
		}
		if len(goods) == 0 {
			log.Fatal("There were no results found.")
		}
		for name, val := range goods {
			fmt.Printf("%s: %f\n", name, val)
		}
	},
}
